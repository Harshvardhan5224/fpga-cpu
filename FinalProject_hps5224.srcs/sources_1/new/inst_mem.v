`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:36:37 PM
// Design Name: 
// Module Name: inst_mem
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module inst_mem(pc, inst);
    input[0:31] pc;
    output reg[0:31] inst;
    reg[0:31]IM[0:511];
    initial begin
        IM[100] = 32'b10001100001000100000000000000000;
        IM[104] = 32'b10001100001000110000000000000100;
        IM[108] = 32'b10001100001001000000000000001000;
        IM[112] = 32'b10001100001001010000000000001100;
        IM[116] = 32'b00000000010010100011000000100000;
    end
    always@(pc)
    begin
        inst <= IM[pc];
    end
endmodule
