`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:36:37 PM
// Design Name: 
// Module Name: id_exe
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module id_exe(rs,rt,wreg,m2reg,wmem,aluc,aluimm,mux_out,qa,qb,eximm,clk,ewreg,em2reg,ewmem,ealuc,ealuimm,emux_out,eqa,eqb,eeximm,ers,ert);
    input wreg,m2reg,wmem,aluimm;
    input[0:3] aluc;
    input[0:4] mux_out;
    input[0:31] qa,qb,eximm;
    input[4:0] rs,rt;
    input clk;
    output reg ewreg,em2reg,ewmem,ealuimm;
    output reg[0:3] ealuc;
    output reg[0:4] emux_out;
    output reg[0:31] eqa,eqb,eeximm;
    output reg[4:0] ers,ert;
    always@(posedge clk)
    begin
        ewreg <= wreg;
        em2reg <= m2reg;
        ewmem <= wmem;
        ealuimm <= aluimm;
        ealuc<=aluc;
        emux_out <=mux_out;
        eqa <= qa;
        eqb <= qb;
        eeximm <= eximm;
        ers <= rs;
        ert <= rt;
    end
endmodule
