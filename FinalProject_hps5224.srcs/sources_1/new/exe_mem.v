`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:36:37 PM
// Design Name: 
// Module Name: exe_mem
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module exe_mem(clk, ewreg, em2reg, ewmem, emux_out, r, eqb, mwreg,mm2reg,mwmem, mmux_out, mr, mqb);

   
    input clk, ewreg,em2reg, ewmem;
    input[4:0] emux_out;
    input[31:0] r,eqb;
    output reg mwreg,mm2reg, mwmem;
    output reg[4:0] mmux_out;
    output reg[31:0] mr,mqb;
    always@(posedge clk)
    begin
        mwreg <= ewreg;
        mm2reg <= em2reg;
        mwmem <= ewmem;
        mmux_out <= emux_out;
        mr <= r;
        mqb <= eqb;
    end
endmodule
