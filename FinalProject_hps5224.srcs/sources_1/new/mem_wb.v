`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:36:37 PM
// Design Name: 
// Module Name: mem_wb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module mem_wb(clk, mwreg,mm2reg,mmux_out, mr, do, wwreg, wm2reg, wmux_out, wr, wdo);
    
    input clk, mwreg, mm2reg;
    input[4:0] mmux_out;
    input[31:0] mr, do;
    output reg wwreg,wm2reg;
    output reg[4:0] wmux_out;
    output reg[31:0] wr,wdo;
 
    always@(posedge clk)
    begin
        wwreg <= mwreg;
        wm2reg <= mm2reg;
        wmux_out <= mmux_out;
        wr <= mr;
        wdo <= do;
    end
endmodule
