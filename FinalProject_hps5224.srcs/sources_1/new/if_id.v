`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:22:51 PM
// Design Name: 
// Module Name: if_id
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module if_id(in, clk,rs,rt,rd,func,op,imm);
    input[31:0] in;
    input clk;
    output reg[31:0] imm;
    output reg[4:0] rs,rt,rd;
    output  reg[5:0] func,op;
    always@(posedge clk)
    begin
        op = in[31:26];
        func = in[5:0];
        rd = in[15:11];
        rs = in[25:21];
        rt = in[20:16];   
        imm = in[15:0];
    end
endmodule
 
