`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:36:37 PM
// Design Name: 
// Module Name: data_mem
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module data_mem(we, a, di, do);
    // Initializing Registers
    input[31:0] a,di;
    input we;
    output reg[31:0] do;
    reg[31:0]data_memory[0:511];
    
   
    initial begin
        data_memory[0] <= 32'hA00000AA;
        data_memory[4] <= 32'h10000011;
        data_memory[8] <= 32'h20000022;
        data_memory[12] <= 32'h30000033;
        data_memory[16] <= 32'h40000044;
        data_memory[20] <= 32'h50000055;
        data_memory[24] <= 32'h60000066;
        data_memory[28] <= 32'h70000077;
        data_memory[32] <= 32'h80000088;
        data_memory[36] <= 32'h90000099;
    end
    always@(a,di)
    begin
        do <= data_memory[a];
    end
endmodule
