`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:36:37 PM
// Design Name: 
// Module Name: control_unit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module control_unit(op, func, wreg, m2reg, wmem, aluc, aluimm, regrt);

    input[5:0] op, func;
    output reg wreg, m2reg,wmem, aluimm, regrt;
    output reg[3:0] aluc;
    always@(op, func)
    begin
        case(op)
            6'b100011://LW
            begin
                wreg <= 1'b1;
                m2reg <= 1'b1;
                wmem <= 1'b0;
                aluimm <= 1'b1;
                regrt <= 1'b1;
                aluc <= 4'b0010;
            end
            6'b000000://R-type
            begin
                if(func== 6'b100000)
                begin
                    wreg <= 1'b1;
                    m2reg <= 1'b0;
                    wmem <= 1'b0;
                    aluimm <= 1'b0;
                    regrt <= 1'b0;
                    aluc <= 4'b0010;
                end
            end
            6'b101011://SW
            begin
                wreg <= 1'b0;
                m2reg <= 1'b0;
                wmem <= 1'b1;
                aluimm <= 1'b1;
                regrt <= 1'bx;
                aluc <= 4'b0010;
            end
            6'b101011://beq
            begin
                wreg <= 1'b0;
                m2reg <= 1'bx;
                wmem <= 1'b0;
                aluimm <= 1'b0;
                regrt <= 1'bx;
                aluc <= 4'b0110;
            end
        endcase
    end
endmodule
