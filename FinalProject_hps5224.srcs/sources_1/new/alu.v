`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:36:37 PM
// Design Name: 
// Module Name: alu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module alu(ealuc, eqa, b, r);
    
    input[3:0] ealuc;
    input[31:0] eqa,b;
    output reg[31:0] r;
    
    always@(*)
    begin
        case(ealuc)
            4'b0010:r = eqa + b;
            4'b0001:r = eqa | b;
            4'b0111:r = eqa - b;
            4'b0000:r = eqa & b;
            4'b0011:r = eqa ^ b;
        endcase
    end
endmodule
