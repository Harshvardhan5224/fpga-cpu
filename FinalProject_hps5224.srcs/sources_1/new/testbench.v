`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:36:37 PM
// Design Name: 
// Module Name: testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module testbench();
    
    // Clock
    reg main_clk;
    initial begin 
        main_clk = 0;
        
        #150 $finish;
    end
    always
    begin
        #5 main_clk = ~main_clk;
    end
    
    //Initializing wires and registers
    wire[31:0] pc_in, pc_out, inst;
    wire[5:0] op, func;
    wire wreg;
    wire m2reg;
    wire wmem;
    wire[3:0] aluc;
    wire aluimm;
    wire regrt;
    wire[4:0] mux_out;
    wire[4:0] rd, rs, rt;
    wire[31:0] qa,qb;
    wire[31:0] imm,iimm; 
    wire ewreg;
    wire em2reg;
    wire ewmem;
    wire[3:0] ealuc;
    wire ealuimm;   
    wire[31:0] eqa,eqb;
    wire[31:0] eimm;
    
    wire[31:0] b,r;
    
    wire mwreg;
    wire mm2reg;
    wire mwmem;  
    wire[31:0] mr,mqb;
    wire[4:0] ern,mrn;
    
    wire[31:0] do;
    
    wire wwreg, wm2reg;
    wire[4:0] wmux_out;
    wire[31:0] wr, wdo;
    wire[31:0] mux3out;
    wire[1:0] fwda,fwdb;
    wire[31:0] muxa,muxb;
    wire[4:0] ers,ert;
    // Creating components
    
     pc PC(pc_in, main_clk, pc_out);
     adder A(pc_out, pc_in);
     inst_mem IM(pc_out, inst);
     if_id IF_ID(inst, main_clk, rs,rt,rd,func,op,iimm);
     control_unit CU(op, func, wreg, m2reg, wmem, aluc, aluimm, regrt);    
     mux2x4 MF1(qa,r,mr,fwda,muxa);
     mux4x2B MF2(qb,r,mr,fwdb,muxb);
     mux M(rd,rt,regrt,mux_out);
     reg_file RF(main_clk,rs,rt,wwreg,wmux_out,mux3out,qa,qb);
     extender E(iimm,imm);
     id_exe ID_EXE(rs,rt,wreg,m2reg,wmem,aluc,aluimm,mux_out,muxa,muxb,iimm,main_clk,ewreg,em2reg,ewmem,ealuc,ealuimm,ern,eqa,eqb,eimm,ers,ert);
     mux2 M2(ealuimm,eqb,eimm, b);
     alu ALU(ealuc, eqa, b, r);
     exe_mem EXE_MEM(main_clk, ewreg, em2reg, ewmem, ern, r, eqb, mwreg,mm2reg,mwmem, mrn, mr, mqb);
     data_mem DATA_MEMORY(mwmem, mr, mqb, do);
     mem_wb MEM_WB(main_clk, mwreg,mm2reg,mrn, mr, do, wwreg, wm2reg, wmux_out, wr, wdo);
     mux3 M3(wm2reg, wr, wdo, mux3out);
     
endmodule 
