`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:36:37 PM
// Design Name: 
// Module Name: mux3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module mux3(wm2reg, r, do, mux3out);
    input[31:0] r, do;
    input wm2reg;
    output reg[31:0] mux3out;
    always@(wm2reg, r,do)
    begin
        case(wm2reg)
            1'b0: mux3out <= r;
            1'b1: mux3out <= do;
        endcase
    end
endmodule
