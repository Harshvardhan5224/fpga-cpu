`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:36:37 PM
// Design Name: 
// Module Name: mux4x2B
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module mux4x2B(qb,r,mr,fwdb,out);
    input[31:0] qb,r,mr;
    input[1:0] fwdb;
    output reg[31:0] out;
    always@(*)
    begin
        case(fwdb)
            2'b00: out <= qb;
            2'b01: out <= r;
            2'b10: out <= mr;
        endcase
    end
endmodule
