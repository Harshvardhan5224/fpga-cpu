`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:36:37 PM
// Design Name: 
// Module Name: mux2x4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module mux2x4(qa,r,mr,fwda,out);
    input[31:0] qa,r,mr;
    input[1:0] fwda;
    output reg[31:0] out;
    always@(*)
    begin
        case(fwda)
            2'b00: out <= qa;
            2'b01: out <= r;
            2'b10: out <= mr;
        endcase
    end
endmodule
