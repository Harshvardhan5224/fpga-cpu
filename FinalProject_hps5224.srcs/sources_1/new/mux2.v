`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:36:37 PM
// Design Name: 
// Module Name: mux2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module mux2(ealuimm, eqb, eeximm, b);
    
    input[31:0] eqb,eeximm;
    input ealuimm;
    output reg[31:0] b;
    
    always@(*)
    begin
        if(ealuimm)
        begin
            b <= eeximm;
        end
        else
        begin
            b <= eqb;
        end
    end
endmodule
