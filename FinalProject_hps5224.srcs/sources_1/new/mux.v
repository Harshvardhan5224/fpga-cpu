`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:36:37 PM
// Design Name: 
// Module Name: mux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module mux(rd,rt , regrt, out);
    input regrt;
    input[0:4] rd, rt;
    output reg[0:4] out;
    always@(*)
    begin
        if(regrt == 0)
        begin
            out <= rd;            
        end 
        else
        begin
            out <= rt;
        end
    end
endmodule
