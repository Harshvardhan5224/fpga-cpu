`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/14/2020 11:36:37 PM
// Design Name: 
// Module Name: reg_file
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module reg_file(clk, rs, rt, we, wn, d , qa, qb );

    input[4:0] rs,rt, wn;
    input clk,we;
    input[31:0] d;
    output reg[0:31] qa, qb;
    reg[31:0] reg_file[0:31];
    integer i; 
    initial
    begin
  
        
        reg_file[0] <= 32'h00000000;
        reg_file[1] <= 32'hA00000AA;
        reg_file[2] <= 32'h10000011;
        reg_file[3] <= 32'h20000022;
        reg_file[4] <= 32'h30000033;
        reg_file[5] <= 32'h40000044;
        reg_file[6] <= 32'h50000055;
        reg_file[7] <= 32'h60000066;
        reg_file[8] <= 32'h70000077;
        reg_file[9] <= 32'h80000088;
        reg_file[10] <= 32'h90000099;
  
 
    for( i = 0; i <= 31; i = i + 1)
        reg_file[i+11] <= 1'b0;
    end
    always@(*)
    begin
        qa <= reg_file[rs];
        qb <= reg_file[rt];
    end
    always@(negedge clk)
    begin
        case(we)
            1'b1:reg_file[wn] = d;
        endcase
    end
    
endmodule
